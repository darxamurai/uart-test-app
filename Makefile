CFLAGS+= -Wall -Werror -std=gnu99 -g
#LDFLAGS+= -pthread -lm

#CFLAGS+=-DBAUD_RATE=691200 - for the patch1

all: main

OBJS=${patsubst %.c,%.o,${wildcard *.c}}

main: ${OBJS}
	${CC} ${OBJS} ${LDFLAGS} -o $@

${OBJS}: %.o: %.c
	${CC} -c ${CFLAGS} $< -o $@

clean:
	rm -f main ${OBJS}
