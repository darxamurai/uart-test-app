# UART test app

This app works like reciever and transmitter of simple text messsages at the same time.

## To use this app you need to do following steps:
1. Get some USB <-> UART converter and connect it to your Linux computer
2. Make sure that your user is in dialout group (you can see how to set it [here](https://askubuntu.com/questions/112568/how-do-i-allow-a-non-default-user-to-use-serial-device-ttyusb0))
3. Compile the program using the "make" command
4. Launch the program passing the path to your USB device as argument like in example: "./main /dev/ttyUSB0"
5. Repeat previous step for second USB device in other terminal
6. If all steps were made correctly, you can now send messsages from one device to another

## Usage:
First symbol you type will decide how the program will interprete the following symbols:
- Type 0..9 if you want to change the baud rate
- Type 'q' if you want to quit 
- Type some other symbol to start writing down the message you want to send (first symbol won't be sent, it is used only to set the terminal to blocking mode) and then press Return to send

If you made everything correctly, you will see your message in other terminal (you can use both terminals for same purposes)
