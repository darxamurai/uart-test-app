#include <stdio.h>
#include <stdbool.h>

#include <fcntl.h>
#include <unistd.h>
#include <poll.h>
#include <assert.h>
#include <termios.h>

void call_termios(int reset);
int getc_timeout(int fd, int timeout_ms, unsigned char *c);
int set_baud(int fd, speed_t baud_rate);
int open_fd(char *filename);

const speed_t bauds[] = {B1200, B1800, B2400, B4800, B9600, B19200, B38400, B57600, B115200, B230400};

const char* baud_str[] = {"B1200", "B1800", "B2400", "B4800", "B9600", "B19200", "B38400", "B57600", "B115200", "B230400"};

int main(int argc, char *argv[])
{
    char *portName;

    // check if port name is set

    if (argc > 1)
    {
        portName = argv[1];
    }
    else
    {
        fprintf(stderr, "no port set!\n");
        return 0;
    }

    // try to open serial port

    int fd = open_fd(portName);
    if (fd < 0)
    {
        fprintf(stderr, "device is not connected\n");
        return 100;
    }

    bool quit = false;

    call_termios(0);

    while (!quit)
    {
        unsigned char c;
        if (getc_timeout(STDIN_FILENO, 10, &c) == 1)                    // check if something is not typed to stdin for 10 ms
        {

            switch (c)
            {
            case 'q':                                                   // check if q isn't pressed
                fprintf(stderr, "\e[1;31mquit\e[0m\n");
                quit = true;
                break;
            case '1':                                                   // change baud rate
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
            case '0':
                write(fd, &c, 1);
                unsigned char b;
                getc_timeout(fd, 20, &b);
                fd = set_baud(fd, bauds[c - '0']);
                fprintf(stderr, "baud %s set\n", baud_str[c - '0']);
                break;

            default:                                                    // message typed
                call_termios(1);
                char *message = NULL;
                size_t size = 0;
                size = getline(&message, &size, stdin);

                if (size != write(fd, message, size))
                {
                    fprintf(stderr, "message not sent\n");
                }
                call_termios(0);
                break;
            }
        }

        if (getc_timeout(fd, 10, &c) == 1)                              // check if some message recieved
        {
            switch (c)
            {
            case '1':                                                   // baud rate message recieved
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
            case '0':
                fd = set_baud(fd, bauds[c - '0']);
                fprintf(stderr, "\e[1;34mbaud %s set\e[0m\n", baud_str[c - '0']);
                break;
            default:                                                     // usual message recieved
                fprintf(stderr, "\e[1;33m%c\e[0m", c);
                while (getc_timeout(fd, 10, &c) > 0)
                {
                    fprintf(stderr, "\e[1;33m%c\e[0m", c);
                }
                break;
            }
        }
    }

    call_termios(1);
    close(fd);

    return 0;
}

void call_termios(int reset)                                            // this function is used to set terminal to raw mode
{
    static struct termios tio, tioOld;
    tcgetattr(STDIN_FILENO, &tio);
    if (reset)
    {
        tcsetattr(STDIN_FILENO, TCSANOW, &tioOld);
    }
    else
    {
        tioOld = tio; //backup
        cfmakeraw(&tio);
        tio.c_oflag |= OPOST;
        tcsetattr(STDIN_FILENO, TCSANOW, &tio);
    }
}

int getc_timeout(int fd, int timeout_ms, unsigned char *c)              // this function is used to read char from file with given timeout
{
    struct pollfd ufdr[1];
    int r = 0;
    ufdr[0].fd = fd;
    ufdr[0].events = POLLIN | POLLRDNORM;
    if ((poll(&ufdr[0], 1, timeout_ms) > 0) && (ufdr[0].revents & (POLLIN | POLLRDNORM)))
    {
        r = read(fd, c, 1);
    }
    return r;
}

int set_baud(int fd, speed_t baud_rate)                                 // this function sets the baud rate
{
    struct termios attr;
    tcgetattr(fd, &attr);

    cfsetospeed(&attr, baud_rate);
    cfsetispeed(&attr, baud_rate);

    tcsetattr(fd, TCSADRAIN, &attr);
    tcflush(fd, TCIOFLUSH);

    return fd;
}

int open_fd(char *filename)                                             // this function is used to open the file
{
    int fd = open(filename, O_RDWR | O_NOCTTY | O_SYNC);

    struct termios attr;
    tcgetattr(fd, &attr);
    cfmakeraw(&attr);
    tcsetattr(fd, TCSANOW, &attr);

    return fd;
}